using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Runtime.InteropServices;

public class ProofOfConcept : MonoBehaviour
{
   private Texture2D cameraTexture;
   private List<byte> currentFrameData = new List<byte>();
   private Mesh unityMesh;

    #if UNITY_IOS
        [DllImport("__Internal")]
        private static extern string MeshingPlugin_runNativeCode();
        // [DllImport("__Internal")]
        // private static extern void MeshingPlugin_startCamera();
        // [DllImport("__Internal")]
        // private static extern void MeshingPlugin_processFrame(IntPtr framePointer, int length, int width, int height);
        // [DllImport("__Internal")]
        // private static extern void MeshingPlugin_Meshroom();
    #endif
    // Start is called before the first frame update
    void Start()
    {
        #if UNITY_IOS
        MeshingPlugin_runNativeCode();

        // Initialize a Unity mesh
        unityMesh = new Mesh();
        GetComponent<MeshFilter>().mesh = unityMesh;
        #else
        Debug.Log("iOS functionality only");
        #endif
    }

   void Update() {
      #if UNITY_IOS
      if (currentFrameData.Count > 0) {
         byte[] frameBytes = currentFrameData.ToArray();

         // Resize and load data into the texture
         cameraTexture.Reinitialize(frameBytes.Length / 4, 1);
         cameraTexture.LoadRawTextureData(frameBytes);
         cameraTexture.Apply();

         Renderer renderer = GetComponent<Renderer>();
         if (renderer != null) {
            renderer.material.mainTexture = cameraTexture;
         }

         currentFrameData.Clear();
      }
      #endif
   }

   public void ProcessFrame(byte[] frameData, int width, int height) {
      currentFrameData.Clear();
      currentFrameData.AddRange(frameData);
   }

    public void UpdateMesh(byte[] verticesData, byte[] facesData) {
      #if UNITY_IOS
      // Convert vertex data to an array of Vector3
      Vector3[] vertices = new Vector3[verticesData.Length / 12];
      for (int i = 0; i < vertices.Length; i++) {
         vertices[i] = new Vector3(
            BitConverter.ToSingle(verticesData, i * 12),
            BitConverter.ToSingle(verticesData, i * 12 + 4),
            BitConverter.ToSingle(verticesData, i * 12 + 8)
         );
      }

      // Convert face data to an array of integers
      int[] triangles = new int[facesData.Length / 4];
      for (int i = 0; i < triangles.Length; i++) {
         triangles[i] = BitConverter.ToInt32(facesData, i * 4);
      }

      // Update the Unity mesh
      unityMesh.Clear();
      unityMesh.vertices = vertices;
      unityMesh.triangles = triangles;
      #endif
   }
}
