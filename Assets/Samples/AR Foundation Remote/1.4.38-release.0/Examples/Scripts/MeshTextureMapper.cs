using System;
using System.Collections.Generic;
using ARFoundationRemoteExamples;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System.IO;
using System.Linq;
using System.Text;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.XR.CoreUtils;
using UnityEngine.XR.ARSubsystems;

public class MeshTextureMapper : MonoBehaviour
{
    public ARMeshManager meshManager;
    public SetupARFoundationVersionSpecificComponents captureManager;
    public ARSessionOrigin sessionOrigin;
    public Camera arCamera;
    public int textureWidth = 1920;
    public int textureHeight = 1440;
    public float widthUV = 1.0f;
    public float heightUV = 1.0f;
    public int counter = 0;
    private List<Vector2[]> capturedUVs = new List<Vector2[]>();
    private int captureIndex = 0;
    private bool uvsAssigned = false;
       void Start()
       { 
            meshManager.meshesChanged += OnMeshesChanged;
            InvokeRepeating(nameof(CaptureAndDump), 1, 1);  
       } 
       public static void ExportMeshToObj(Mesh mesh, string filePath, string mtlFileName)
        {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine($"mtllib {mtlFileName}");

        foreach (Vector3 v in mesh.vertices)
        {
            sb.AppendLine($"v {v.x} {v.y} {v.z}");
        }

        foreach (Vector3 v in mesh.normals)
        {
            sb.AppendLine($"vn {v.x} {v.y} {v.z}");
        }

        foreach (Vector2 v in mesh.uv)
        {
            sb.AppendLine($"vt {v.x} {v.y}");
        }

        sb.AppendLine("usemtl material_0");
        sb.AppendLine("usemap material_0");

        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            sb.AppendLine($"f {mesh.triangles[i + 0] + 1}/{mesh.triangles[i + 0] + 1}/{mesh.triangles[i + 0] + 1} " +
                          $"{mesh.triangles[i + 1] + 1}/{mesh.triangles[i + 1] + 1}/{mesh.triangles[i + 1] + 1} " +
                          $"{mesh.triangles[i + 2] + 1}/{mesh.triangles[i + 2] + 1}/{mesh.triangles[i + 2] + 1}");
        }

        File.WriteAllText(filePath, sb.ToString());
    }
    public static void ExportMaterialToMtl(string filePath, string textureFileName)
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine("newmtl material_0");
        sb.AppendLine("Ka 1.000 1.000 1.000");
        sb.AppendLine("Kd 1.000 1.000 1.000");
        sb.AppendLine("Ks 0.000 0.000 0.000");
        sb.AppendLine("d 1.0");
        sb.AppendLine("illum 2");
        sb.AppendLine($"map_Kd {textureFileName}");

        File.WriteAllText(filePath, sb.ToString());
    }
    
    void OnMeshesChanged(ARMeshesChangedEventArgs eventArgs)
    {
        Debug.Log("Mesh has changed");
        Texture2D capturedTexture = captureManager.capturedTextures.Last();
        Debug.Log("This is the length of the event args added " + eventArgs.added.Count.ToString());
        Debug.Log("This is the length of the event args updated " + eventArgs.updated.Count.ToString());
        foreach (var meshFilter in eventArgs.updated)
        {
            ApplyTextureToMesh(meshFilter, capturedTexture);
        }
        
    }
    
    private void ApplyTextureToMesh(MeshFilter meshFilter, Texture2D texture)
    {
        var meshRenderer = meshFilter.GetComponent<MeshRenderer>();
        
        if (meshRenderer != null)
        {
            if (meshRenderer.material == null)
            {
                meshRenderer.material = new Material(Shader.Find("Standard"));
            }
            meshRenderer.material.mainTexture = texture;
        }

        // Calculate and apply UVs
        Mesh mesh = meshFilter.mesh;
        Vector2[] uvs = CalculateUVs(meshFilter, arCamera);
        mesh.uv = uvs;
        capturedUVs.Add(mesh.uv);
        uvsAssigned = true;

    }

    public static Mesh CombineMeshes(List<MeshFilter> meshFilters)
    {
        List<CombineInstance> combine = new List<CombineInstance>();
        foreach (var meshFilter in meshFilters)
        {
            CombineInstance ci = new CombineInstance();
            ci.mesh = meshFilter.sharedMesh;
            ci.transform = meshFilter.transform.localToWorldMatrix;
            combine.Add(ci);
        }

        Mesh combinedMesh = new Mesh();
        combinedMesh.CombineMeshes(combine.ToArray(), true, true);
        return combinedMesh;
    }
    public void ExportMeshAndTexture()
    {
        List<Texture2D> capturedTextures = captureManager.GetCapturedTextures();
        List<Matrix4x4> cameraPoses = captureManager.cameraPoses;
        
        string objFilePath = Path.Combine(Application.persistentDataPath, "exportedMesh.obj");
        string mtlFilePath = Path.Combine(Application.persistentDataPath, "exportedMesh.mtl");
        string textureFilePath = Path.Combine(Application.persistentDataPath, "capturedTexture.png");
        
        if (capturedTextures.Count > 0 && capturedTextures != null)
        {
            List<MeshFilter> meshFilters = new List<MeshFilter>(meshManager.meshes);
            Texture2D stitchedTexture = ProjectTexturesOntoMesh(capturedTextures, cameraPoses, meshFilters , textureWidth, textureHeight);

            Mesh combinedMesh = CombineMeshes(meshFilters);
            ExportMeshToObj(combinedMesh, objFilePath, "exportedMesh.mtl");
            ExportMaterialToMtl(mtlFilePath, "capturedTexture.png");
            SaveTextureToFile(stitchedTexture, textureFilePath);

            Debug.Log("Mesh and texture exported successfully.");
        }
        else
        {
            Debug.LogError("No meshes found to export.");
        }
    }
    private Vector2[] CalculateUVs(MeshFilter meshFilter, Camera virtCamera)
    {
        Matrix4x4 cameraMatrix = arCamera.worldToCameraMatrix;
        Matrix4x4 projectionMatrix = arCamera.projectionMatrix;

        Vector2[] uvs = CalculateUVs(meshFilter, virtCamera, cameraMatrix, projectionMatrix);
        Debug.Log("Calculating UVs");
        // if (counter % 3000 == 0)
        // {
        //     CaptureAndDump();
        //     
        // }
        // else
        // {
        //     counter++;
        // }
        return uvs;
    }
public Texture2D ProjectTexturesOntoMesh(List<Texture2D> textures, List<Matrix4x4> poses, List<MeshFilter> meshFilters, int textureWidth, int textureHeight)
    {
        Color[] finalTexturePixels = new Color[textureWidth * textureHeight];
        float[] pixelWeights = new float[textureWidth * textureHeight];
        

        for (int i = 0; i < textures.Count; i++)
        {
            Texture2D texture = textures[i];
            Matrix4x4 pose = poses[i];
            Color[] texturePixels = texture.GetPixels();
            foreach (var meshFilter in meshFilters)
            {
                Mesh mesh = meshFilter.mesh;
                Vector3[] vertices = mesh.vertices;
                Transform meshTransform = meshFilter.transform;
                
                    for (int j = 0; j < vertices.Length; j++)
                    {

                        Vector3 worldPosition = meshTransform.TransformPoint(vertices[j]);
                        Vector3 localPosition = pose.inverse.MultiplyPoint3x4(worldPosition);

                        Vector2 uv = new Vector2(localPosition.x / textureWidth, localPosition.y / textureHeight);

                        if (uv.x >= 0 && uv.x <= 1 && uv.y >= 0 && uv.y <= 1)
                        {
                            int x = Mathf.FloorToInt(uv.x * texture.width);
                            int y = Mathf.FloorToInt(uv.y * texture.height);

                            int pixelIndex = y * texture.width + x;
                            int finalPixelIndex = j * textureWidth + (int)(uv.x * textureWidth);

                            if (finalPixelIndex >= 0 && finalPixelIndex < finalTexturePixels.Length &&
                                pixelIndex >= 0 && pixelIndex < texturePixels.Length)
                            {
                                finalTexturePixels[finalPixelIndex] += texturePixels[pixelIndex];
                                pixelWeights[finalPixelIndex] += 1;
                            }
                        }
                    }
            }
        }

        for (int j = 0; j < finalTexturePixels.Length; j++)
        {
            if (pixelWeights[j] > 0)
            {
                finalTexturePixels[j] /= pixelWeights[j];
            }
        }

        Texture2D finalTexture = new Texture2D(textureWidth, textureHeight);
        finalTexture.SetPixels(finalTexturePixels);
        finalTexture.Apply();
        return finalTexture;
    }

    private void SaveTextureToFile(Texture2D texture, string filePath)
    {
        for (int x = 0; x < textureWidth; x++)
        {
            for (int y = 0; y < 1; y++)
            {
                SetAlphaToZero(texture, x, y); // Bottom border
                SetAlphaToZero(texture, x, textureHeight - 1 - y); // Top border
            }
        }

        for (int y = 0; y < textureHeight; y++)
        {
            for (int x = 0; x < 1; x++)
            {
                SetAlphaToZero(texture, x, y); // Left border
                SetAlphaToZero(texture, textureWidth - 1 - x, y); // Right border
            }
        }

        // Apply the changes to the texture
        texture.Apply();
        byte[] bytes = texture.EncodeToPNG();
        System.IO.File.WriteAllBytes(filePath, bytes);
        Debug.Log($"Texture saved to {filePath}");
    }
    private static void SetAlphaToZero(Texture2D texture, int x, int y)
    {
        Color color = texture.GetPixel(x, y);
        color.a = 0f;
        texture.SetPixel(x, y, color);
    }
    private void CaptureAndDump()
    {
        if(uvsAssigned)
        {
            Texture2D texture = captureManager.capturedTextures.Last();
            Transform cameraTransform = sessionOrigin.Camera.transform;
            var rotatedTexture = RotateTextureBasedOnCameraTransform(texture, cameraTransform);


            string textureFilePath = Path.Combine(Application.persistentDataPath, $"texture_{captureIndex}.png");
            SaveTextureToFile(rotatedTexture, textureFilePath);

            List<MeshFilter> meshFilters = new List<MeshFilter>(meshManager.meshes);
            if (meshFilters.Count > 0)
            {
                
                List<MeshFilter> viewedMesh = GetVisibleMeshes(meshFilters, cameraTransform);
                Mesh combinedMesh = CombineMeshes(viewedMesh);
                string objFilePath = Path.Combine(Application.persistentDataPath, $"mesh_{captureIndex}.obj");
                string mtlFilePath = Path.Combine(Application.persistentDataPath, $"mesh_{captureIndex}.mtl");
                MeshExporter.ExportMeshToObj(combinedMesh, objFilePath, $"mesh_{captureIndex}.mtl");
                MeshExporter.ExportMaterialToMtl(mtlFilePath, $"texture_{captureIndex}.png");


                captureIndex++;
                Debug.Log("Captured and dumped mesh and texture.");
                uvsAssigned = false;
            }
        }
    }
    private Texture2D RotateTextureBasedOnCameraTransform(Texture2D originalTexture, Transform cameraTransform)
    {
        // Determine the device orientation based on the camera's transform
        Vector3 cameraForward = cameraTransform.forward;
        Vector3 cameraUp = cameraTransform.up;

        if (Vector3.Dot(cameraUp, Vector3.up) > 0.5f)
        {
            // Portrait
            //return originalTexture;
            //I should be returning portrait- I am returning portrait, but I am rotated 90 degrees, so I will return
            // this instead
            
            return RotateTexture90(originalTexture, false);
        }
        else if (Vector3.Dot(cameraUp, Vector3.down) > 0.5f)
        {
            // Portrait Upside Down
            return RotateTexture180(originalTexture);
        }
        else if (Vector3.Dot(cameraForward, Vector3.right) > 0.5f)
        {
            // Landscape Left
            return RotateTexture90(originalTexture, true);
        }
        else if (Vector3.Dot(cameraForward, Vector3.left) > 0.5f)
        {
            // Landscape Right
            return RotateTexture90(originalTexture, false);
        }

        // Default case
        return originalTexture;
    }

    private static Texture2D RotateTexture90(Texture2D originalTexture, bool clockwise)
    {
        int width = originalTexture.width;
        int height = originalTexture.height;
        Texture2D rotatedTexture = new Texture2D(height, width);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (clockwise)
                {
                    rotatedTexture.SetPixel(j, width - i - 1, originalTexture.GetPixel(i, j));
                }
                else
                {
                    rotatedTexture.SetPixel(height - j - 1, i, originalTexture.GetPixel(i, j));
                }
            }
        }

        rotatedTexture.Apply();
        return rotatedTexture;
    }

    private static Texture2D RotateTexture180(Texture2D originalTexture)
    {
        int width = originalTexture.width;
        int height = originalTexture.height;
        Texture2D rotatedTexture = new Texture2D(width, height);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                rotatedTexture.SetPixel(width - i - 1, height - j - 1, originalTexture.GetPixel(i, j));
            }
        }

        rotatedTexture.Apply();
        return rotatedTexture;
    }
    private List<MeshFilter> GetVisibleMeshes(List<MeshFilter> meshFilters, Transform cameraTransform)
    {
        List<MeshFilter> visibleMeshes = new List<MeshFilter>();

        Plane[] frustumPlanes = GeometryUtility.CalculateFrustumPlanes(cameraTransform.GetComponent<Camera>());

        foreach (MeshFilter meshFilter in meshFilters)
        {
            if (GeometryUtility.TestPlanesAABB(frustumPlanes, meshFilter.mesh.bounds))
            {
                visibleMeshes.Add(meshFilter);
            }
        }

        return visibleMeshes;
    }
    public Vector2[] CalculateUVs(MeshFilter meshFilter, Camera ProjectionPerspective, Matrix4x4 cameraMatrix, Matrix4x4 projectionMatrix)
    {
        Vector3[] vertices = meshFilter.mesh.vertices;
        //ProjectionPerspective.projectionMatrix = ManualProjectionMatrix(-widthUV, widthUV, -heightUV, heightUV, ProjectionPerspective.nearClipPlane, ProjectionPerspective.farClipPlane);
        Vector2[] uvs = new Vector2[vertices.Length];
        Vector2 ScreenPosition;
        for (int i = 0; i < uvs.Length; i++) {
            ScreenPosition = ProjectionPerspective.WorldToViewportPoint(meshFilter.transform.TransformPoint(vertices[i]));
            uvs[i].Set(ScreenPosition.x, ScreenPosition.y);
        }
        // for (int i = 0; i < vertices.Length; i++)
        // {
        //     Vector3 vertex = vertices[i];
        //     Vector3 cameraSpacePos = cameraMatrix.MultiplyPoint3x4(vertex);
        //     Vector3 projectedPos = projectionMatrix.MultiplyPoint(cameraSpacePos);
        //
        //     float u = (projectedPos.x + 1) / 2;
        //     float v = (projectedPos.y + 1) / 2;
        //
        //     uvs[i] = new Vector2(u, v);
        // }

        return uvs;
    }
    static Matrix4x4 ManualProjectionMatrix(float left, float right, float bottom, float top, float near, float far) 
    {
        float x = 2.0F * near / (right - left);
        float y = 2.0F * near / (top - bottom);
        float a = (right + left) / (right - left);
        float b = (top + bottom) / (top - bottom);
        float c = -(far + near) / (far - near);
        float d = -(2.0F * far * near) / (far - near);
        float e = -1.0F;
        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = x;
        m[0, 1] = 0;
        m[0, 2] = a;
        m[0, 3] = 0;
        m[1, 0] = 0;
        m[1, 1] = y;
        m[1, 2] = b;
        m[1, 3] = 0;
        m[2, 0] = 0;
        m[2, 1] = 0;
        m[2, 2] = c;
        m[2, 3] = d;
        m[3, 0] = 0;
        m[3, 1] = 0;
        m[3, 2] = e;
        m[3, 3] = 0;
        return m;
    }
}