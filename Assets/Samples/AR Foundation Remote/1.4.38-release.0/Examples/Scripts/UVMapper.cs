using UnityEngine;

public class UVMapper:MonoBehaviour
{
    public float width = 0.2f;
    public float height = 0.2f;
    

    public Vector2[] CalculateUVs(Vector3[] vertices, Camera ProjectionPerspective, Matrix4x4 cameraMatrix, Matrix4x4 projectionMatrix)
    {
        ProjectionPerspective.projectionMatrix = ManualProjectionMatrix(-width, width, -height, height, ProjectionPerspective.nearClipPlane, ProjectionPerspective.farClipPlane);
        Vector2[] uvs = new Vector2[vertices.Length];
        Vector2 ScreenPosition;
        for (int i = 0; i < uvs.Length; i++) {
            ScreenPosition = ProjectionPerspective.WorldToViewportPoint(transform.TransformPoint(vertices[i]));
            uvs[i].Set(ScreenPosition.x, ScreenPosition.y);
        }
        // for (int i = 0; i < vertices.Length; i++)
        // {
        //     Vector3 vertex = vertices[i];
        //     Vector3 cameraSpacePos = cameraMatrix.MultiplyPoint3x4(vertex);
        //     Vector3 projectedPos = projectionMatrix.MultiplyPoint(cameraSpacePos);
        //
        //     float u = (projectedPos.x + 1) / 2;
        //     float v = (projectedPos.y + 1) / 2;
        //
        //     uvs[i] = new Vector2(u, v);
        // }

        return uvs;
    }
    static Matrix4x4 ManualProjectionMatrix(float left, float right, float bottom, float top, float near, float far) 
    {
        float x = 2.0F * near / (right - left);
        float y = 2.0F * near / (top - bottom);
        float a = (right + left) / (right - left);
        float b = (top + bottom) / (top - bottom);
        float c = -(far + near) / (far - near);
        float d = -(2.0F * far * near) / (far - near);
        float e = -1.0F;
        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = x;
        m[0, 1] = 0;
        m[0, 2] = a;
        m[0, 3] = 0;
        m[1, 0] = 0;
        m[1, 1] = y;
        m[1, 2] = b;
        m[1, 3] = 0;
        m[2, 0] = 0;
        m[2, 1] = 0;
        m[2, 2] = c;
        m[2, 3] = d;
        m[3, 0] = 0;
        m[3, 1] = 0;
        m[3, 2] = e;
        m[3, 3] = 0;
        return m;
    }

    
}