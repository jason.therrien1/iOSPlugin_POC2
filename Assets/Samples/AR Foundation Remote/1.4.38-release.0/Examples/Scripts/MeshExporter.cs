using System.IO;
using System.Text;
using UnityEngine;

public class MeshExporter
{
    public static void ExportMeshToObj(Mesh mesh, string filePath, string mtlFileName)
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine($"mtllib {mtlFileName}");

        foreach (Vector3 v in mesh.vertices)
        {
            sb.AppendLine($"v {v.x} {v.y} {v.z}");
        }

        foreach (Vector3 v in mesh.normals)
        {
            sb.AppendLine($"vn {v.x} {v.y} {v.z}");
        }

        foreach (Vector2 v in mesh.uv)
        {
            sb.AppendLine($"vt {v.x} {v.y}");
        }

        sb.AppendLine("usemtl material_0");
        sb.AppendLine("usemap material_0");

        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            sb.AppendLine($"f {mesh.triangles[i + 0] + 1}/{mesh.triangles[i + 0] + 1}/{mesh.triangles[i + 0] + 1} " +
                          $"{mesh.triangles[i + 1] + 1}/{mesh.triangles[i + 1] + 1}/{mesh.triangles[i + 1] + 1} " +
                          $"{mesh.triangles[i + 2] + 1}/{mesh.triangles[i + 2] + 1}/{mesh.triangles[i + 2] + 1}");
        }

        File.WriteAllText(filePath, sb.ToString());
    }
    public static void ExportMaterialToMtl(string filePath, string textureFileName)
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine("newmtl material_0");
        sb.AppendLine("Ka 1.000 1.000 1.000");
        sb.AppendLine("Kd 1.000 1.000 1.000");
        sb.AppendLine("Ks 0.000 0.000 0.000");
        sb.AppendLine("d 1.0");
        sb.AppendLine("illum 2");
        sb.AppendLine($"map_Kd {textureFileName}");

        File.WriteAllText(filePath, sb.ToString());
    }
}